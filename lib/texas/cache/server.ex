defmodule Texas.Cache.Server do

  use GenServer
  @topic_prefix "texas:diff:"
  @topic_prefix_length String.length(@topic_prefix)

  def start_link(args) do
    GenServer.start_link(__MODULE__, [state: args[:state], uid: args[:uid]])
  end

  def init(opts) do
    case Registry.lookup(CacheRegistry, opts[:uid]) do
      [] ->
        Registry.register(CacheRegistry, opts[:uid], []) # |> IO.inspect label: "trying to register #{opts[:uid]}"
        sub_to_props(opts[:state])
        state = Map.merge(opts[:state], %{uid: opts[:uid]})
        {:ok, state}
      _ ->
        :ignore
    end
  end

  def handle_call({:update_socket, socket}, _from, state) do
    state = put_in(state[:socket], socket)
    #IO.inspect socket, label: "putting in socket"
    {:reply, :ok, state}
  end

  defp sub_to_props(state) do
    pubsub = Application.get_env(:texas, :pubsub)
    case state[:data] do
      nil ->  nil
      _ -> Enum.each(state[:data], fn {k, _v} -> pubsub.subscribe("#{@topic_prefix}#{k}") end)
    end
  end

  def handle_info(%{topic: <<_topic::bytes-size(@topic_prefix_length), data_attr::binary>>}, old_state) do
    data_attr = String.to_atom(data_attr)
    socket = old_state.socket

    with {view_module, cached_fragment} <- get_cached(data_attr, old_state),
         new_fragment                   <- apply(view_module, data_attr, [socket]),
         {:ok, diff}                    <- Texas.Diff.diff(data_attr, cached_fragment, new_fragment)
    do
      send(old_state.socket.channel_pid, {:diff, diff})
      new_state = put_in(old_state[:data][data_attr], new_fragment)
      {:noreply, new_state}
    else
      _ -> {:noreply, old_state}
    end
  end

  def handle_info(_any, state) do
    {:noreply, state}
  end

  defp get_cached(data_attr, state) do
    %{view: view_module, data: view_data} = state
    {:ok, cached_fragment} = Map.fetch(view_data, data_attr)

    {view_module, cached_fragment}
  end
end

defmodule WorkingOnDocsWeb.Router do
  use WorkingOnDocsWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    #plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Texas.Plug
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", WorkingOnDocsWeb do
    pipe_through :browser # Use the default browser stack

    get "/", TodoController, :index
    post "/update", TodoController, :update
  end

  # Other scopes may use custom stacks.
  # scope "/api", WorkingOnDocsWeb do
  #   pipe_through :api
  # end
end

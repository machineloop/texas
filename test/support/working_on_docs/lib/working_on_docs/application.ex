defmodule WorkingOnDocs.Supervisor do
  use Application
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec

    # Define workers and child supervisors to be supervised
    children = [
      supervisor(WorkingOnDocsWeb.Endpoint, []),
      %{id: PersistenceProcess, start: {Agent, :start_link, [ fn -> [] end, [name: SomePersistenceLayer]]}},
      %{id: TodoStateProcess, start: {Agent, :start_link, [ fn -> :all end, [name: TodoListState]]}}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: WorkingOnDocs.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    WorkingOnDocsWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
